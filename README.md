# A Community-driven Collection and Template for DMP Guidance facilitating Data Management across Disciplines and Funding

Data management plans (DMPs) are formal and structured documents that capture all relevant information along the data lifecycle and beyond. A DMP typically describes data handling, archiving, sharing, access, usage and interpretation.
A DMP instance applies a certain DMP template in order to address a specific funder or institution for a given use case. However, the structure of existing DMP templates is often designed for funderâs requirements and is consequently generic in scope to address diverse research projects and communities. Thus, these templates often lack guidance on how to structure discipline-specific information in a DMP and information on related consequences, when not following recommended steps. Moreover, additional guidance is often provided in consultations or as unstructured text in specific DMP tools or separate documents and is therefore not prepared for discovery or reuse.
To address these challenges, we propose to structure DMP guidance by applying a pattern concept. The pattern concept (see Alexander C, Ishikawa S, Silverstein M, et al (1977) A Pattern Language. Oxford University Press, New York) was developed to collect proven solutions for frequently occurring problems. First patterns covered architecture elements, like specific window forms for specific house forms. By now, the pattern concept is well-established in several disciplines, in particular as software design patterns (see Gamma E, Helm R, Johnson R, Vlissides J (1995) Design Patterns: Elements of Reusable Object-Oriented Software. Addison Wesley Professional Computing Series) tackling frequently occurring IT problems and providing solution blueprints. 
A pattern provides a structure to describe the mentioned pair of problem and solution. Thus, a pattern describes the problem, the tested solution, requirements, which link problem and solution, the context, the rationale, and the pragmatics of the provided problem and solution. Moreover, a pattern is contextualized, i.e. the described problem occurs in a specific context or the solution can only be applied in specific context. Several patterns can therefore describe different solutions for the same problem in different contexts.
Patterns are typically shipped in collections, e.g. with a certain thematic or technical focus. With the help of the given pattern structure - the pattern attributes - users are enabled to search/filter a collection for patterns that fit to their use case(s). 
The overall aims of the pattern concept are facilitating reuse of established / well-known solutions (best practices), collection and exchange of knowledge and experiences. Thus, such a pattern concept - providing a generic and common structure for describing problems and solutions -  serves as the basis for our proposed guidance pattern template.

With our proposed DMP guidance template, we provide a common structure for DMP guidance facilitating 
1. DMP writing across disciplines, 
2. the provision of concrete examples (best practices), and 
3. the provision of consequences to foster awareness for certain data management topics.

We describe the general proposed structure for describing DMP guidance as patterns, which can be used to develop further patterns. We then discuss a community-driven collection of patterns in an open repository and provide selected examples covering common or disciplinary DMP guidance pattern examples. Each of the described pattern examples is based on needs, experiences, or implementations from existing research projects supported by the document's authors.

## Community-driven Collection
To provide expert knowledge for data management and specific guidance, we propose the community-driven collection and quality-assurance of guidance patterns. We therefore provide an open repository for DMP guidance patterns that facilitate experts in adding new patterns, updating patterns, e.g. with new best practice project examples, and commenting existing patterns.

[Guidance Pattern Gitlab Repo](https://git.rwth-aachen.de/ub-it/DMP_Guidance_Pattern) 

The concept of providing supplementary information to generic DMP templates in a modular way is not new. For instance, the Digital Curation Centre (DCC) developed a guidance for recurring template topics  and institutions can provide further guidance for their users within DCCâs DMPonline by grouping their recommendations and best practices according to the DMP themes.  
However, with our DMP pattern concept and the provided repository, we foster reusing condensed and harmonized guidance and examples by linking from DMP tools, like RDMO , or other related knowledge bases. Moreover, we enable an update mechanism for guidance that is independent from the linked DMP tool by separating DMP tool and guidance information management. Thus, we support domain experts that are not familiar with specific DMP tools syntax to provide their guidance or comments via open GitLab repository as structured text.
By using the GitLab issue tracking, we enable commenting patterns and envision a community-driven update and quality-assurance process, similar to the Wiki approach. 
Future work will improve the guidance pattern discovery, e.g. by using the given tagging/classification and linking the repository to discovery tools.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support & Contribution
Contribute to the project by providing feedback to the suggested template or by adding your DMP Guidance Patterns in our repository. 

## Roadmap
We focus a community-driven process and a regular update of the template and provided examples

## Authors and acknowledgment
Christin Henzen (christin.henzen@tu-dresden.de), Jürgen Rohrwild, Daniela Hausen, Karsten Peters-von Gehlen, Ivonne Anders, Sabine Schönau.
The authors thank the RDMO group to provide for organisational support.

## License
tbd
 

